from trytond.pool import Pool, PoolMeta
from trytond.model.exceptions import AccessError
from trytond.i18n import gettext

class Account(metaclass=PoolMeta):
    __name__ = 'account.account'

    @classmethod
    def __setup__(cls):
        super(Account, cls).__setup__()
        cls.party_required.domain = [()]
        cls.party_required.states = {}

class Line(metaclass=PoolMeta):
      __name__ = 'account.move.line'
      @classmethod
      def __setup__(cls):
          super(Line, cls).__setup__()
          cls.party.states = {}

      def check_account(self):
          if not self.account.type or self.account.closed:
              raise AccessError(
                      gettext('account.msg_line_closed_account',
                          account=self.account.rec_name))
          if self.account.party_required:
              if bool(self.party) != bool(self.account.party_required):
                  error = 'party_set' if self.party else 'party_required'
                  raise AccessError(
                        gettext('account.msg_line_%s' % error,
                            account=self.account.rec_name,
                            line=self.rec_name))

