# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool  import PoolMeta


class Category(metaclass=PoolMeta):
    __name__ = "product.category"
    products = fields.Many2Many('product.template-product.category.all', 
            'category', 'template', string='Products')
    machine = fields.Char('Machine')
