# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import sale, category, account



__all__ = ['register']


def register():
    Pool.register(
        sale.Sale,    
        sale.SaleLine,
        category.Category,
        account.Account,
        account.Line,
        module='sale_invoice_line_extra', type_='model')
    Pool.register(
        module='sale_invoice_line_extra', type_='wizard')
    Pool.register(
        module='sale_invoice_line_extra', type_='report')
