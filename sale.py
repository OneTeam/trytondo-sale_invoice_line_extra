# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.exceptions import UserError
from trytond.model import fields
from trytond.pool  import PoolMeta, Pool
from trytond.pyson import Eval

class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    root_analytic = fields.Many2One('analytic_account.account','Root',
            domain=[
                ('childs.type', '=', 'normal'),
                ],
            depends=['childs']
             )
    account_analytic = fields.Many2One('analytic_account.account',
            'Analytic Account', required=True,
            domain=[
                ('parent', '=', Eval('root_analytic'))
                ],
            depends=['root_analytic']
            )
class SaleLine(metaclass=PoolMeta):
    __name__= 'sale.line'

    model = fields.Many2One('product.category','Model',
            domain=[
                ('products.products', '=', Eval('product'))
                ],
                        depends=['product']
            )
    category = fields.Char('Category')
    machine = fields.Char('Machine')

    @fields.depends('product','model','analytic_accounts',
            '_parent_sale.account_analytic','_parent_sale.root_analytic',
            'unit', 'sale', '_parent_sale.party', '_parent_sale.invoice_party',
            methods=['compute_taxes', 'compute_unit_price',
            'on_change_with_amount'])
    def on_change_product(self):
        if not self.product:
            return

        party = None
        if self.sale:
            party = self.sale.invoice_party or self.sale.party

        # Set taxes before unit_price to have taxes in context of sale price
        self.taxes = self.compute_taxes(party)

        category = self.product.sale_uom.category
        if not self.unit or self.unit.category != category:
            self.unit = self.product.sale_uom
            self.unit_digits = self.product.sale_uom.digits

        self.unit_price = self.compute_unit_price()

        self.type = 'line'
        self.amount = self.on_change_with_amount()
        if self.product:
            self.category = self.product.account_category.name
        
        self.analytic_accounts = ()
        try:
            self.analytic_accounts += ({'root': self.sale.account_analytic.root, 
                'account': self.sale.account_analytic},) 
        except:
            raise UserError(str("Debe seleccionar 'Ciudad' y 'Sede'"))
            
    @fields.depends('model', 'machine')
    def on_change_model(self):
        if self.model:
            self.machine = self.model.machine
  
