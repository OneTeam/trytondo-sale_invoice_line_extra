try:
    from trytond.modules.sale_invoice_line_extra.tests.test_sale_invoice_line_extra import suite  # noqa: E501
except ImportError:
    from .test_sale_invoice_line_extra import suite

__all__ = ['suite']
