import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class SaleInvoiceLineExtraTestCase(ModuleTestCase):
    'Test Sale Invoice Line Extra module'
    module = 'sale_invoice_line_extra'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            SaleInvoiceLineExtraTestCase))
    return suite
